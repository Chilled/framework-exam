const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const pathToRegexp = require('path-to-regexp');
const app = express();
const checkJwt = require('express-jwt');
const connectionString = 'mongodb+srv://admin:admin@cluster0-qgrxp.mongodb.net/jobs?retryWrites=true&w=majority';
mongoose.connect(connectionString, { useNewUrlParser: true }, (err) => { console.log('mongo db connection', err) });
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../build')));
process.env.JWT_SECRET = 'O7fD22KTN5rZmR5lOivs0f6okj';

/****** Configuration *****/
const port = (process.env.PORT || 8080);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("DB connection is open!");
});


if (!process.env.JWT_SECRET) {
    console.error('You need to put a secret in the JWT_SECRET env variable!');
    process.exit(1);
}
// Additional headers to avoid triggering CORS security errors in the browser
// Read more: https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

    // intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        // respond with 200
        res.sendStatus(200);
    } else {
        // move on
        next();
    }
});

//Open paths that does not need login
const jobPath_api = pathToRegexp('/api/jobs/job/:id');
const searchPath_api = pathToRegexp('/api/jobs/find/cat/:category/city/:city');
const searchPath_react = pathToRegexp('/find/cat/:category/city/:city')
const jobPath_react = pathToRegexp('/job/:id')
let openPaths = [
    '/api/users/authenticate',
    '/api/jobs/city',
    '/api/jobs/category',
    '/api/jobs',
    '/api/users/signup',
    jobPath_api,
    searchPath_api,
    searchPath_react,
    jobPath_react
];
// Validate the user using authentication
app.use(
    checkJwt({ secret: process.env.JWT_SECRET }).unless({ path : openPaths})
);
app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({ error: err.message });
    }
});

app.get('/api/test', (req,res) =>{
    res.json({msg: 'HIT'});
});
let jobsRouter = require('./jobs_router')();
app.use('/api/jobs', jobsRouter);
let usersRouter = require('./users_router')();
app.use('/api/users', usersRouter);

/****** Error handling ******/

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send({ msg: 'Something broke!' })
});

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});


app.listen(port, () => console.log(`API running on port ${port}!`));

