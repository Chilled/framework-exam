module.exports = () =>{
    const express = require('express');
    const router = express.Router();
    const mongoose = require('mongoose');


    /****** Data *****/
    let categorySchema = new mongoose.Schema({
        category: String,
    });
    let citySchema = new mongoose.Schema({
        city: String
    });
    let jobSchema = new mongoose.Schema({
        title: String,
        description: String,
        category: String,
        city: String
    });

    let JobModel = mongoose.model('Job', jobSchema);
    let CityModel = mongoose.model('City', citySchema);
    let CategoryModel = mongoose.model('Category', categorySchema);

    /****** Routes *****/
    router.get('/', (req, res) => {
        JobModel.find({}, (err, job) => {
            res.json(job);
        })
    });

    router.post('/create', (req, res) => {
        let newJob = new JobModel(req.body);
        newJob.save();
        res.status(200).json(`Posted`);
    });

    router.get('/job/:id', (req, res) => {
        console.log(req.params.id)
        JobModel.findOne({ _id: req.params.id }, (err, job) => {
            res.json(job);
        })
    });

    router.get('/find/cat/:category/city/:city', (req, res) => {
        JobModel.find({ category: req.params.category, city: req.params.city}, (err, job) => {
            res.json(job)
        })
    });

    router.get('/city', (req,res) =>{
        CityModel.find({}, (err, city) =>{
            res.json(city);
        })
    });
    router.post('/city', (req, res) =>{
        let newCity = new CityModel(req.body);
        newCity.save();
        res.status(200).json(`City posted`);
    });
    router.get('/category', (req,res) =>{
        CategoryModel.find({}, (err, category) =>{
            res.json(category);
        })
    });
    router.post('/category', (req, res) =>{
        console.log(req.body);
        let newCategory = new CategoryModel(req.body);
        newCategory.save();
        res.status(200).json(`Category posted`);
    });

    return router;
};