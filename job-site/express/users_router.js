module.exports = () => {
    const express = require('express');
    const router = express.Router();
    const mongoose = require('mongoose');

    const jwt = require('jsonwebtoken');
    const bcrypt = require('bcrypt');
    const saltRounds = 10;


    let userSchema = new mongoose.Schema({
        username: String,
        password: String
    });

    let userModel = new mongoose.model('Users', userSchema);


    router.post('/signup', (req, res) => {
        // TODO: Implement user account creation
        console.log('User post hit');
        const username = req.body.username;
        const password = req.body.password;
        console.log(req.body)
        let cryptedPass = "nothing";

        bcrypt.hash(password, saltRounds, function (err, hash) {
            cryptedPass = hash;
            console.log(cryptedPass);
            const newUser = new userModel(
                {
                    username: username,
                    password: cryptedPass,
                }
            );
            newUser.save((err) => {
                if (err)
                    console.log(err)
                // res.status(500).send(err);
                else
                    res.status(200).send(`User registration success`);
            })
        });
    });

    router.put('/', (req, res) => {
        // TODO: Implement user update (change password, etc).
        res.status(501).json({ msg: "PUT update user not implemented" });
    });

    router.post('/authenticate', (req, res) => {
        const username = req.body.username;
        const password = req.body.password;

        if (!username || !password) {
            let msg = "Username or password missing!";
            console.error(msg);
            res.status(401).json({ msg: msg });
            return;
        }

        let user;
        userModel.findOne({ username: username }, (err, users) => {
            user = users;
        }).then(user => {
            if (user) {
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {
                        const payload = {
                            username: username,
                            admin: false
                        };
                        const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h' });

                        res.json({
                            msg: 'User authenticated successfully',
                            token: token
                        });
                    } else res.status(401).json({ msg: "Password mismatch!" })
                });
            } else {
                res.status(404).json({ msg: "User not found!" });
            }
        })
    });

    return router;
};