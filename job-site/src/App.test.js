import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {render} from 'react-testing-library';


it('renders App with header text', () => {
  const {getByText} = render(<App/>);
  expect(getByText(/Job-site/i)).toBeInTheDocument();
});