import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {render} from 'react-testing-library';
import JobList from './jobList';


it('renders jobList with text', () => {
    const comp = 
        <Router>
            <JobList jobs={jobTestData} />
        </Router>
  const {getByText} = render(comp);
  expect(getByText('Microsoft Dynamics Developer - Group IT')).toBeInTheDocument();
  expect(getByText('Bunker Holding is looking for a Microsoft Dynamics Developer to join its new software development team in Athens.')).toBeInTheDocument();
});