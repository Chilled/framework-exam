import React, { Component } from 'react';

class AddCategory extends Component {
    constructor(props){
        super(props);

        this.state = {
            category: ''
        }
        this.onChange = this.onChange.bind(this)
        this.addCategory = this.addCategory.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }
    componentDidMount(){
        this.handleLogin();
    }

    onChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    addCategory(e){
        e.preventDefault();
        console.log(this.state.category);
        this.props.addCategory(this.state);
        this.props.history.push('/create')
    }

    handleLogin(){
        if(!this.props.auth.loggedIn())
            this.props.history.push('/signin');
    }

    render() {
        return (
            <div>
                <strong>Opret ny kategori</strong>
                <form>
                    <input type={'text'} id={'category'} onChange={this.onChange} placeholder={'Ny kategori'} autoComplete={'off'} />
                </form>
                <button onClick={this.addCategory}>Opret kategori</button>
            </div>
        )
    }
}
export default AddCategory;



//  <strong>Opret ny kategori</strong>
//  <form>
//      <input type={'text'} id={'newCategory'} onChange={this.onChange} placeholder={'Ny kategori'} />
//  </form>
//  <button onClick={this.addCategory}>Opret kategori</button>