import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Signin extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: "",
            password: ""
        }
        this.onChange = this.onChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    onChange(e){
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleLogin(e){
        e.preventDefault();
        this.props.login(this.state.username, this.state.password)
        this.props.history.push('/')
    }


    render() {
        return (
            <div>
                <form>
                    <input type="text" placeholder="brugernavn" id={'username'} onChange={this.onChange} />
                    <br />
                    <input type="password" placeholder="password" id={'password'} onChange={this.onChange} />
                    <br />
                    <button onClick={this.handleLogin}>login</button>
                    <br />
                    <Link to={'/signup'} className={'signupLink'}>Eller opret en bruger...</Link>
                </form>
            </div>
        )
    }
}

export default Signin;