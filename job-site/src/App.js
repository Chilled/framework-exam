import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import './App.scss';
import JobList from './jobList';
import JobAd from './jobAd';
import Job from './job';
import SortJob from './sortJob';
import AuthService from './AuthService';
import Signup from './signup';
import Signin from './signin';
import AddCategory from './addCategory';
import AddCity from './addCity';

class App extends Component {

    constructor(props) {
        super(props);
        this.api_url = process.env.REACT_APP_API_URL;
        this.Auth = new AuthService(`${this.api_url}/users/authenticate`);
        this.state = {
            jobList: [],
            city: [],
            category: [],
            job: [],
            loggedIn: false
        };

        this.addJob = this.addJob.bind(this);
        this.getCities = this.getCities.bind(this);
        this.getCategories = this.getCategories.bind(this);
        this.getJob = this.getJob.bind(this);
        this.login = this.login.bind(this);
        this.signout = this.signout.bind(this);
        this.addCategory = this.addCategory.bind(this)
        this.addCity = this.addCity.bind(this);
        this.signup = this.signup.bind(this);
    }

    componentDidMount() {
        this.fetchAllJobs();
        this.getCities();
        this.getCategories();
    }

    fetchAllJobs() {
        this.Auth.fetch(`${this.api_url}/jobs`)
            .then(jobs => {
                this.setState({
                    jobList: jobs
                })
            })
    }

    addJob(json) {
        this.Auth.fetch(`${this.api_url}/jobs/create`, {
            method: 'POST',
            body: JSON.stringify(json)
        })
            .then(result => {
                this.fetchAllJobs();
            })
    }

    addCategory(json) {
        this.Auth.fetch(`${this.api_url}/jobs/category`, {
            method: 'POST',
            body: JSON.stringify(json)
        })
            .then(result => {
                this.getCategories();
            })
    }
    addCity(json) {
        this.Auth.fetch(`${this.api_url}/jobs/city`, {
            method: 'POST',
            body: JSON.stringify(json)
        })
            .then(result => {
                console.log(result)
                console.log('addcity')
                this.getCities();
            })
    }

    getJob(id) {
        console.log(id)
        this.Auth.fetch(`${this.api_url}/jobs/job/${id}`)
            .then(json => {
                console.log(json)
                this.setState({
                    job: json
                })
            })
    }

    getCities() {
        this.Auth.fetch(`${this.api_url}/jobs/city`)
            .then(json => {
                this.setState({
                    city: json
                })
            })
    }

    getCategories() {
        this.Auth.fetch(`${this.api_url}/jobs/category`)
            .then(json => {
                this.setState({
                    category: json
                })
            })
    }

    login(username, password) {
        console.log(username, password);
        this.Auth.login(
            username,
            password
        )
            .then(response => {
                this.setState({
                    loggedIn: true
                })

            })
            .catch(error => {
                console.error("Error authenticating:", error);
            });
    }

    signout() {
        this.Auth.logout();
        this.setState({
            loggedIn: false
        })
    }

    signup(json){
        console.log(json);
        fetch(`${this.api_url}/users/signup`, {
            method: 'POST',
            body: JSON.stringify(json)
        }).then(response => response.json())
        .then(json =>{
            
        })
    }

    render() {
        let signin = <Link to={'/signin'} className={'signout'}>Login</Link>;
        if (this.Auth.loggedIn()) {
            signin = <button onClick={this.signout} className={'signout'} >Signout</button>;

        }
        return (
            <Router>
                <div className={'grid-container'}>
                    <div className={'grid-header'}>
                        <div className={'content-center'}>
                            <h1>Job-site</h1>
                            {signin}
                        </div>
                    </div>
                    <nav className={'grid-nav'}>
                        <div className={'content-center'}>
                            <ul>
                                <li>
                                    <Link to={'/'}>jobs</Link>
                                </li>
                                <li>
                                    <Link to={'/create'}>opret jobannonce</Link>
                                </li>
                                <li>
                                    <Link to={'/category'}>opret kategori</Link>
                                </li>
                                <li>
                                    <Link to={'/city'}>opret by</Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <section className={'grid-content'}>
                        <div className={'content-center'}>
                            <Switch>
                                <Route exact path={'/'}
                                    render={(props) =>
                                        <JobList {...props} jobs={this.state.jobList} cities={this.state.city}
                                            categories={this.state.category} />} />

                                <Route exact path={'/create'}
                                    render={(props) =>
                                        <JobAd {...props} addJobAd={this.addJob} cities={this.state.city}
                                            categories={this.state.category} auth={this.Auth} />} />
                                <Route exact path={'/category'}
                                    render={(props) =>
                                        <AddCategory {...props} addCategory={this.addCategory} auth={this.Auth} />} />
                                <Route exact path={'/city'}
                                    render={(props) =>
                                        <AddCity {...props} addCity={this.addCity} auth={this.Auth} />} />
                                <Route exact path={'/job/:id'}
                                    render={(props) =>
                                        <Job {...props} api_url={this.api_url} getJob={this.getJob}
                                            job={this.state.job} />} />
                                <Route exact path={'/find/cat/:category/city/:city'}
                                    render={(props) =>
                                        <SortJob {...props} />} />
                                <Route exact path={'/signup'}
                                    render={(props) =>
                                        <Signup {...props} signup={this.signup} />} />
                                <Route exact path={'/signin'}
                                    render={(props) =>
                                        <Signin {...props} login={this.login} />} />
                            </Switch>
                        </div>
                    </section>
                </div>
            </Router>
        )
    }
}

export default App;
