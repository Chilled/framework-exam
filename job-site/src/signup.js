import React, { Component } from 'react';

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        }
        this.onChange = this.onChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    onChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleLogin(e) {
        e.preventDefault();
        this.props.signup(this.state)
        this.props.history.push('/')
    }


    render() {
        return (
            <div>
                <strong>Opret bruger er midlertidig under udvikling</strong>
                <br />
                <em>Du kan logge ind med <strong>Bruger: </strong>test og <strong>kode:</strong> test</em>
            </div>
            // <div>
            //     <form>
            //         <input type="text" placeholder="brugernavn" id={'username'} onChange={this.onChange} autoComplete={'off'}  />
            //         <br />
            //         <input type="password" placeholder="password" id={'password'} onChange={this.onChange} autoComplete={'off'} />
            //         <br />
            //         <button onClick={this.handleLogin}>Opret</button>
            //     </form>
            // </div>
        )
    }
}

export default Signup;