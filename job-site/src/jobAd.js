import React, { Component } from 'react';
import { Link } from "react-router-dom";

class JobAd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            description: "",
            category: "it",
            city: "aarhus",
            isLoggedIn: false,
        };

        this.onChange = this.onChange.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    componentDidMount() {
        this.handleLogin();
    }

    onChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleLogin() {
        let loggedIn = this.props.auth.loggedIn();
        this.setState({
            isLoggedIn: loggedIn
        });
    }

    handleInput(e) {
        e.preventDefault();
        this.props.addJobAd(this.state);
        this.props.history.push('/')
    }

    render() {
        let cities = [];
        let categories = [];
        if (this.props.cities && this.props.categories) {
            this.props.cities.forEach(city => {
                cities.push(
                    <option value={city.city} key={city._id}>
                        {city.city}
                    </option>
                )
            });

            this.props.categories.forEach(category => {
                categories.push(
                    <option value={category.category} key={category._id}>
                        {category.category}
                    </option>
                )
            })
        }
        if (this.state.isLoggedIn)
            return (
                <div>
                    <form>
                        <input type={'text'} id={'title'} placeholder={'title'} autoComplete={'off'} onChange={this.onChange} />
                        <textarea placeholder={'beskrivelse'} id={'description'} onChange={this.onChange}>
                        </textarea>
                        <select id={'category'} onChange={this.onChange}>
                            {categories}
                        </select>
                        <select id={'city'} onChange={this.onChange}>
                            {cities}
                        </select>
                    </form>
                    <button onClick={this.handleInput}>Offenligør annonce</button>
                   
                </div>
            )
        else
            return (
                <div>
                    Du er ikke logget ind....
                    <br />
                    <Link to={'/signin'}>Login</Link>
                </div>
            )
    }
}

export default JobAd;