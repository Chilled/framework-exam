import React, {Component} from 'react';
import {Link} from "react-router-dom";

class JobList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            category: "",
            city: "",
        }

        this.onChange = this.onChange.bind(this);
        this.click = this.click.bind(this);
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    click() {
        console.log(this.state)
    }

    render() {
        let jobs = [];
        let cities = [];
        let categories = [];
        if (this.props.jobs.length > 0)
            this.props.jobs.forEach((job, index) => {
                jobs.push(
                    <div key={index} className={'jobLink'}>
                        <Link to={`job/${job._id}`} >
                            <strong>
                                {job.title}
                            </strong>
                            <p>
                                {job.description}
                            </p>
                            <label>Category: {job.category}</label>
                            <label>city: {job.city}</label>
                        </Link>
                    </div>
                )
            });

        if (this.props.cities && this.props.categories) {
            this.props.cities.forEach(city => {
                cities.push(
                    <li key={city._id}>
                        <input type="radio" name="city" value={city.city} id={city.city} onChange={this.onChange}/>
                        <label htmlFor={city.city}>{city.city}</label>
                    </li>
                )
            })
            this.props.categories.forEach(category => {
                categories.push(
                    <li key={category._id}>
                        <input type="radio" name="category" value={category.category} id={category.category}
                               onChange={this.onChange}/>
                        <label htmlFor={category.category}>{category.category}</label>
                    </li>
                )
            })
        }


        return (
            <div>
                <div className={'list'}>
                    {jobs}
                </div>
                <div className={'sort'}>
                    <strong>By</strong>
                    <ul>
                        {cities}
                    </ul>
                    <strong>Kategory</strong>
                    <ul>
                        {categories}
                    </ul>
                    <Link to={`/find/cat/${this.state.category}/city/${this.state.city}`}>Søg</Link>
                </div>
            </div>
        )
    }
}

export default JobList;