// react-testing-library renders your components to document.body,
// this will ensure they're removed after each test.
import 'react-testing-library/cleanup-after-each';
// this adds jest-dom's custom matchers
import 'jest-dom/extend-expect'
// some global test data for all your tests
global.jobTestData = [{
    title: "Microsoft Dynamics Developer - Group IT",
    description: "Bunker Holding is looking for a Microsoft Dynamics Developer to join its new software development team in Athens.",
    category: "it",
    city: "aarhus"
},
{
    title: "Senior Microsoft Dynamics CRM Developer",
    description: "For the past 140 years, Bunker Holding has served global shipping and has earned its place among the pioneers in bunkering.",
    category: "it",
    city: "odense"
}]