import React, { Component } from 'react';
import { Link } from "react-router-dom";

class SortJob extends Component {
    api_url = process.env.REACT_APP_API_URL;

    constructor(props) {
        super(props);
        this.state = {
            jobs: [],
            noJob: 'Der er ingen jobs, der passer til dine kriterier.'
        };
        this.fetchJob = this.fetchJob.bind(this);
    }

    componentDidMount() {
        this.fetchJob();
        console.log(this.props.match.params.category);
        console.log(this.props.match.params.city);
    }

    fetchJob() {
        fetch(`${this.api_url}/jobs/find/cat/${this.props.match.params.category}/city/${this.props.match.params.city}`)
            .then(response => response.json())
            .then(json => {
                console.log(json)
                this.setState({
                    jobs: json
                })
            })
    }

    render() {

        let jobs = [];
        this.state.jobs.forEach((job,index) => {
            jobs.push(
                <div key={index} className={'jobLink'}>
                    <Link to={`/job/${job._id}`} >
                        <strong>
                            {job.title}
                        </strong>
                        <p>
                            {job.description}
                        </p>
                        <label>Category: {job.category}</label>
                        <label>city: {job.city}</label>
                    </Link>
                </div>
            )
        })
        if (this.state.jobs.length > 0)
            return (
                <div>
                    {jobs}
                </div>
            )
        else
            return (
                <div>
                    {this.state.noJob}
                </div>
            )
    }
}

export default SortJob;