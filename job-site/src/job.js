import React, { Component } from 'react';

class Job extends Component {
    componentDidMount() {
        this.props.getJob(this.props.match.params.id);
    }
    render() {

        return (
            <div>
                <h1>
                    {this.props.job.title}
                </h1>
                <p className={'job-description'}>
                    {this.props.job.description}
                </p>
                <section className={'job-info'}>
                    <div>
                        <strong>Lokation</strong>
                        <ul>
                            <li>
                                {this.props.job.city}
                            </li>
                        </ul>
                    </div>
                    <div>
                        <strong>Kategori</strong>
                        <ul>
                            <li>
                                {this.props.job.category}
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        )
    }
}

export default Job;