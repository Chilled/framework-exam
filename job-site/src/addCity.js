import React, { Component } from 'react';

class AddCity extends Component {
    constructor(props) {
        super(props);

        this.state = {
            city: ''
        }
        this.onChange = this.onChange.bind(this)
        this.addCity = this.addCity.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }
    componentDidMount(){
        this.handleLogin();
    }
    onChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    addCity(e) {
        e.preventDefault();
        this.props.addCity(this.state);
        this.props.history.push('/create')
    }

    handleLogin(){
        if(!this.props.auth.loggedIn())
            this.props.history.push('/signin');
    }

    render() {
        return (
            <div>
                <strong>Opret ny by</strong>
                <form>
                    <input type={'text'} id={'city'} onChange={this.onChange} placeholder={'Ny by'} autoComplete={'off'} />
                </form>
                <button onClick={this.addCity}>Opret by</button>
            </div>
        )
    }
}

export default AddCity;